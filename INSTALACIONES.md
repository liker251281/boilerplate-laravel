## Instalación de formateador de codigo
```sh
composer require friendsofphp/php-cs-fixer --dev
```
Creo el archivo de reglas:
```sh
touch .php-cs-fixer.php
```
## Instalación de analizador de código estático
```sh
composer require nunomaduro/larastan:^2.0 --dev
```
## Instalación de libreria para ver los logs del sistema
```sh
composer require opcodesio/log-viewer
```
Creo los archivos de configuración de la libreria:
```sh
php artisan vendor:publish --tag="log-viewer-config"
```

## Instalación y configuración de telescope para development
```sh
composer require laravel/telescope --dev
```
Publico los assets de la libreria:
```sh
php artisan telescope:install
php artisan migrate
```
## Instalación y configuración de telescope para development
```sh
composer require darkaonline/l5-swagger
php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
php artisan l5-swagger:generate
```
## Instalación y configuración laravel-modules
```sh
composer require nwidart/laravel-modules
php artisan vendor:publish --provider="Nwidart\Modules\LaravelModulesServiceProvider" --tag="config"
php artisan vendor:publish --provider="Nwidart\Modules\LaravelModulesServiceProvider" --tag="stubs"
```
## Instalación y configuración laravel-data
```sh
composer require spatie/laravel-data
php artisan vendor:publish --provider="Spatie\LaravelData\LaravelDataServiceProvider" --tag="data-config"
```
## Instalación y configuración breeze
```sh
composer require laravel/breeze --dev
php artisan breeze:install
php artisan migrate
```
